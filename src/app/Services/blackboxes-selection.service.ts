import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const BASE_GROUP_URL =  '/assets/data/ex_data.json';

@Injectable({
  providedIn: 'root'
})
export class BlackboxesSelectionService {

  constructor(private http: HttpClient) { }

  public getBlackboxesSelectionData() {
    const url = BASE_GROUP_URL;
    return this.http.get(url);
  }

  public getLearnData(url) {
    return this.http.get(url);
  }
}
