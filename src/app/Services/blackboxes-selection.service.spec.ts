import { TestBed } from '@angular/core/testing';

import { BlackboxesSelectionService } from './blackboxes-selection.service';

describe('BlackboxesSelectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BlackboxesSelectionService = TestBed.get(BlackboxesSelectionService);
    expect(service).toBeTruthy();
  });
});
