export interface BlackboxesSelectionData {
  device_groups: Array<{}>;
  protocols: Array<{}>;
  times: Array<{}>;
}
