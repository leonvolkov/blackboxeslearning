import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { BlackboxesSelectionPageComponent } from './blackboxes-selection-page/blackboxes-selection-page.component';
import { GroupSelectionListComponent } from './group-selection-list/group-selection-list.component';
import { BlackboxesSelectionService } from './Services/blackboxes-selection.service';
import { GroupSelectionComponent } from './group-selection/group-selection.component';

@NgModule({
  declarations: [
    AppComponent,
    BlackboxesSelectionPageComponent,
    GroupSelectionListComponent,
    GroupSelectionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    BlackboxesSelectionService
],
  bootstrap: [AppComponent]
})
export class AppModule { }
