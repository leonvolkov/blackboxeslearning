import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-group-selection',
  templateUrl: './group-selection.component.html',
  styleUrls: ['./group-selection.component.scss']
})
export class GroupSelectionComponent implements OnInit {
  @Input() groupData: Array<{}> = [];
  @Output() sendGroupToUpdate = new EventEmitter<any>();
  public listOpened = false;
  public groupSelected = false;

  constructor() { }

  ngOnInit() {
  }

  public toggleList() {
    this.listOpened = !this.listOpened;
  }

  public toggleGroupSelection(event) {
    this.groupSelected = !this.groupSelected;
    for (const group of this.groupData['devices']) {
      if (this.parseBool(group['active'])) {
        group['checked'] = event.target.checked;
      }
    }
    this.onGroupSelectionChange();
  }

  public onGroupSelectionChange() {
    let selected = true;
    for (const group of this.groupData['devices']) {
      if (this.parseBool(group['active']) && !group['checked']) {
        selected = false;
        break;
      }
    }
    this.groupSelected = selected;
    this.sendGroupToUpdate.emit(this.groupData);
  }

  public onGroupInputClicked(event) {
    event.stopPropagation();
  }

  public parseBool(b) {
    return !(/^(false|0|off|no)$/i).test(b) && !!b;
  }
}
