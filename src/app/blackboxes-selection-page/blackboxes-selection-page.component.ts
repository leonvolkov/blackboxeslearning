import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { BlackboxesSelectionService } from '../Services/blackboxes-selection.service';
import { BlackboxesSelectionData } from '../Services/blackboxes-selection-data';

@Component({
  selector: 'app-blackboxes-selection-page',
  templateUrl: './blackboxes-selection-page.component.html',
  styleUrls: ['./blackboxes-selection-page.component.scss']
})
export class BlackboxesSelectionPageComponent implements OnInit {
  private subscriptions = new Subscription();
  private backUpData: BlackboxesSelectionData;
  public deviceGroups: Array<{}> = [];
  public protocols: Array<{}> = [];
  public times: Array<{}> = [];
  public learnQueryString = '';

  constructor(
    private blackboxesSelectionService: BlackboxesSelectionService
  ) { }

  ngOnInit() {
    this.subscriptions.add(this.blackboxesSelectionService.getBlackboxesSelectionData()
      .subscribe(this.setBlackboxesSelectionData.bind(this)));
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  private setBlackboxesSelectionData(data: BlackboxesSelectionData) {
    this.backUpData = JSON.parse(JSON.stringify( data));
    this.deviceGroups = data.device_groups;
    this.protocols = data.protocols;
    this.times = data.times;
    this.learnQueryString = '';
  }

  public updateGroups(data) {
    this.deviceGroups = data;
    this.setLearnQueryString();
  }

  public onProtocolSelectionChange(event, protocol) {
    protocol['checked'] = event.target.checked;
    this.setLearnQueryString();
  }

  public onTimeSelectionChange(event, time) {
    for (const t of this.times) {
      t['checked'] = false;
    }
    time['checked'] = event.target.checked;
    this.setLearnQueryString();
  }
  public onClearClick(event) {
    this.setBlackboxesSelectionData(this.backUpData);
    event.preventDefault();
  }

  private setLearnQueryString() {
    const devices = [];
    const protocols = [];
    let times = '';

    for (const group of this.deviceGroups) {
      for (const device of group['devices']) {
        if (device['checked']) {
          devices.push(device['id']);
        }
      }
    }
    for (const protocol of this.protocols) {
      if (protocol['checked']) {
        protocols.push(protocol['id']);
      }
    }
    for (const time of this.times) {
      if (time['checked']) {
        times = time['id'];
        break;
      }
    }
    if (devices.length > 0 && protocols.length > 0 && times !== '') {
      this.learnQueryString = '?devices=' + devices.join(',') + '&protocols=' + protocols.join(',') + '&times=' + times;
    } else {
      this.learnQueryString = '';
    }
  }

  public onLearnClick(event) {
    window.location.search = this.learnQueryString;
    // this.subscriptions.add(this.blackboxesSelectionService.getLearnData(query)
    //   .subscribe(this.loadLearningData.bind(this)));
  }
  // private loadLearningData(data) {
  //
  // }

}
