import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackboxesSelectionPageComponent } from './blackboxes-selection-page.component';

describe('BlackboxesSelectionPageComponent', () => {
  let component: BlackboxesSelectionPageComponent;
  let fixture: ComponentFixture<BlackboxesSelectionPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackboxesSelectionPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackboxesSelectionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
