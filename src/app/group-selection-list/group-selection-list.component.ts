import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-group-selection-list',
  templateUrl: './group-selection-list.component.html',
  styleUrls: ['./group-selection-list.component.scss']
})
export class GroupSelectionListComponent implements OnInit {
  @Input() groupsData: Array<{}> = [];
  @Output() sendGroupsToPage = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  public sendGroupsToUpdate(data) {
    for (let group of this.groupsData) {
      if (data['id'] === group['id']) {
        group = data;
        break;
      }
    }
    this.sendGroupsToPage.emit(this.groupsData);
  }

}
