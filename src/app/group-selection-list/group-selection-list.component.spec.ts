import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupSelectionListComponent } from './group-selection-list.component';

describe('GroupSelectionListComponent', () => {
  let component: GroupSelectionListComponent;
  let fixture: ComponentFixture<GroupSelectionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupSelectionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupSelectionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
